package com.playstore.selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.xml.xpath.XPath;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class TechnicalEx {
	ChromeDriver d;
	WebDriverWait wait;	
	Actions a;
	
	//Please Type Your Email & Passw0rd
	String name = "Please Type Your Email";
	String pass = "Please Type Your Passw0rd";
	
	
	@BeforeTest
	public void beforeTest() {

		String chromePath = System.getProperty("user.dir")+"\\resources\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", chromePath);
		d = new ChromeDriver();
		d.manage().window().maximize();
		d.get("https://play.google.com/");
		wait = new WebDriverWait(d, 30);
		a = new Actions(d);
		
	}
	
	
	@Test (priority = 1)
	public void gmailLogin() throws InterruptedException {
		
		/*
			Verifying the functionality of adding the first game to user Wishlist
			
			Steps:
				1. Given I opened the Google Chrome Browser.
				2. And I entered the test Url (https://play.google.com/).
				3. And I choose Games from the side category list.
				4. And I select first game.
				5. And I clicked on “Add to my wishlist”.
				6. And I entered the email credentials.
			
			Expected Result: When I click on “Add to my wishlist button”, Then the game should be added to the Wishlist.

			Actual Result: The selected first game is added to user wishlist successfully.

			Priority

			Test Case Type

			Status

			Tester Name

		 */

		d.findElement(By.xpath("//*[@id=\"wrapper\"]/div[1]/div/ul/li[2]/a/span")).click();
		d.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[1]/c-wiz[1]/ul/li[1]/ul/li[4]/a")).click();
		List<WebElement> element = d.findElements(By.className("id-card-list"));
		element.get(0).findElement(By.className("title")).click();
		d.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/c-wiz/wishlist-add/button/span[2]")).click();

		Thread.sleep(1000);
		d.findElement(By.xpath("//*[@id=\"yDmH0d\"]/div[3]/div/div[2]/div[3]/div/button[2]")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@type='email']")));
		d.findElement(By.xpath("//*[@type='email']")).sendKeys(name);
		d.findElement(By.xpath("//*[@id=\"identifierNext\"]/content/span")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='password']/div[1]/div/div[1]/input")));
		d.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input")).sendKeys(pass);
		d.findElement(By.id("passwordNext")).click();
		Thread.sleep(3000);
		d.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[1]/c-wiz[1]/ul/li[1]/ul/li[4]/a")).click();
		
		//From home, Open and select to add the first and last game displayed to the wishlist
		//First Game
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> firstElement = d.findElements(By.className("id-card-list"));	
		firstElement.get(0).findElement(By.className("title")).click();
		d.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/c-wiz/wishlist-add/button/span[2]")).click();


		d.navigate().back();
		Thread.sleep(3000);

		/*
			Verifying the functionality of adding the last game to user Wishlist
			
			Steps:
				1. Given I selected the first game.
				2. And I navigated back to the Games window.
				3. And I scrolled down to last game.
				4. And I selected the last game.
				5. And I clicked on “Add to my wishlist”. 
			
			Expected Result: When I click on “Add to my wishlist button”, Then the game should be added to the Wishlist.
			
			Actual Result: The selected last game is added to user wishlist successfully.
				
			Priority
			
			Test Case Type

			Tester Name
			
			Status

		 */
		//Last Game
		List<WebElement> lastElement = d.findElements(By.className("id-card-list"));
		lastElement.get(lastElement.size()-1).findElement(By.className("title")).click();
		
	}

	@Test (priority = 2)
	public void games() throws InterruptedException 
	{
		
		/*
		Verifying the installation of one of the chosen games from Wishlist

		Steps:
			1. Given I added two games to Wishlist.
			2. And I hovered over “My Wishlist” in the side category list and clicked.
			3. And I select recent one of the two added games.
			4. And I clicked on Install button.
			5. And I choose a device from the devices drop-down list.
			6. And I click on the Install Button. 

		Expected Result: When I click on “Install Button”, Then the game should be installed in the selected device.
		
		Actual Result: The installation process has been run successfully.

		Priority

		Test Case Type

		Status

		Tester Name

		 */
		
		//WishList
		d.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/c-wiz/wishlist-add/button/span[2]")).click();
		d.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[1]/c-wiz[1]/div[2]/ul/li[5]/a")).click();
		Thread.sleep(3000);
		d.navigate().refresh();
		List<WebElement> wishElement1 = d.findElements(By.className("id-card-list"));
		wishElement1.get(0).findElement(By.className("title")).click();
		d.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[2]/c-wiz/div/span/button")).click();
		
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("/html[1]/body[1]/div[4]/iframe[1]")));
		
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='device-selector-button play-button']")));
		d.findElement(By.xpath("//button[@class='device-selector-button play-button']")).click();
		List<WebElement> mobileList = d.findElements(By.className("device-title"));
		int mobileNum = mobileList.size();
		mobileList.get(mobileNum-1).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.id("purchase-ok-button")));
		d.findElement(By.id("purchase-ok-button")).click();
		d.switchTo().defaultContent();
		try {
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("/html[1]/body[1]/div[4]/iframe[1]")));
			wait.until(ExpectedConditions.elementToBeClickable(By.id("close-dialog-button")));
			
			a.click().build().perform();
			
			System.out.println("Succeed");
		} catch (NoSuchElementException exception) {
			System.out.println("Failed");
		}
	}

	
	
	@Test (priority = 3)
	public void review() throws InterruptedException
	{
		
		/*
		Test the functionality of writing a review for the installed game

		Steps:
			1. Given I installed a game.
			2. And I clicked on “Write review”.
			3. And I hovered over the 5th star and clicked on it.
			4. And I filled the review text field and wrote “Test”.
			5. And I submit the review. 
			
		Expected Result: When I submit the review, then user review should be displayed with the taken actions (5th star and “Test” in the text field).

		Actual Result: The user review is displayed with correct data.

		Priority

		Test Case Type

		Status

		Tester Name

		 */
		
		//Review Button
		d.switchTo().defaultContent();
		d.navigate().refresh();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(@class,'LkLjZd ScJHi HPiPcc id-track-click')]")));
		d.findElement(By.xpath("//button[contains(@class,'LkLjZd ScJHi HPiPcc id-track-click')]")).click();
		
		//stars, write, submit
		List<WebElement> frame = d.findElements(By.tagName("iframe"));
		System.out.println(frame.size());
		
		
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//*[@id=\"yDmH0d\"]/iframe[2]")));	
		Thread.sleep(3000);
		d.switchTo().frame(2);
		
		d.findElement(By.xpath("/html/body/div[9]/div[1]/div/div/div/div[1]/div/div/div[3]/div/div[2]/div[2]/div[4]/div/div/div[1]/div[3]/button[5]")).click();
		
		d.findElement(By.xpath("/html/body/div[9]/div[1]/div/div/div/div[1]/div/div/div[3]/div/div[2]/div[2]/div[2]/div/textarea")).sendKeys("Test");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[9]/div[1]/div/div/div/div[1]/div/div/div[3]/div/div[2]/div[2]/div[5]/div[1]/button")));
		
		d.findElement(By.xpath("/html/body/div[9]/div[1]/div/div/div/div[1]/div/div/div[3]/div/div[2]/div[2]/div[5]/div[1]/button")).click();
		
		
		/*
		Verifying the deletion of user review 

		Steps:
			1. Given I wrote a review for installed game.
			2. And the review is displayed.
			3. And I click on Delete Button.

		Expected Result: When I click on Delete Button, Then the user review should be removed.

		Actual Result: User review has been deleted successfully.

		Priority

		Test Case Type

		Status

		Tester Name

		 */
		
		//Back to the page, select to delete the review added
		
		d.switchTo().defaultContent();
		Thread.sleep(3000);
		
		/*
		List<WebElement> zz = d.findElements(By.tagName("div"));
		for (WebElement q : zz) {
			String tex = q.getText();
			System.out.println(tex);
		}
		
		*/
		
		d.findElement(By.xpath("//body[contains(@class,'tQj5Y ghyPEc IqBfM ecJEib EWZcud k8Lt0 EIlDfe cjGgHb d8Etdd LcUz9d')]/div[contains(@class,'QKtxw')]/div[contains(@class,'WpDbMd')]/c-wiz[contains(@class,'I3xX3c drrice')]/div[@class='T4LgNb']/div[@class='ZfcPIb']/div[@class='UTg3hd']/div[@class='JNury Ekdcne']/div[@class='LXrl4c']/div[@jscontroller='v8syQb']/c-wiz[@jsrenderer='aOubeb']/div[contains(@class,'W4P4ne')]/div[@jscontroller='H6eOGe']/div[@class='zc7KVe']/div[@class='d15Mdf bAhLNe']/div[@class='xKpxId zc7KVe']/div[@class='YCMBp GVFJbb']/div[@jsaction='JIbuQc:KGY6b(AU5wNe),lVE7xc(jiLaCd);']/div[1]")).click();
		
	}


	@AfterTest (enabled = false)
	public void afterTest() {
		d.quit();
	}

}
